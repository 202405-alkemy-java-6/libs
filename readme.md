# Librerias para proyectos JAVA

Acá podras encontrar algunas librerias que iremos usando en las clases. 

## Instalación

Para instalar las librerias en el IDE Eclipse debes ir al proyecto y hacer clic con el boton derecho, luego seleccionar Properties >> Java Build Path >> Libreries >> Classpath >> boton Add External JARs, y agregar las librerias (jar) que necesitas.

Para proyectos que necesiten un servidor de aplicaciones, la libreria de conexion a la DB te recomiendo que la agreger en la carpeta LIB del servidor tomcat 9 (que usamos en clases).

